import { ApiClient } from './apiClient'

let client = new ApiClient(process.env.NEXT_PUBLIC_PAYMENT_SERVER)
export default {
  generatePublicKey(data) {
    return client.get('getPublicKey', data)
  },
  payNow(data) {
    return client.post('tokenBasedPayment', data)
  },
}