import React from "react";
import paymentClient from "src/api/paymentClient";
import {
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown
} from "reactstrap";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  NavItem,
  NavLink,
  Nav,
  Progress,
  Table,
  Container,
  Row,
  Col,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup, Label
} from "reactstrap";
// layout for this page
import Header from "components/Headers/Header.js";
import Admin from "layouts/Admin.js";
import "assets/plugins/nucleo/css/nucleo.css";
class AchPaymentWizard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      
      accType: "C",
      accTypeName:"Checking",
      accNo: "",
      routingNumber: "",
      showJsonData: "",
      isAchPayment: false,
      isChecked:false,
      isError:false

    }

  };
  toggleOption = (e) => {
    console.log(e)
    if(e.target.outerText=="Saving")
    {
      this.setState({
     
        accType: "S",
      })

    }
    if(e.target.outerText=="Checking")
    {
      this.setState({
     
        accType: "C",
      })

    }
 

    this.setState({
      accTypeName: e.target.outerText,
    })

  };

  update = (e) => {
    if (e.target.name == 'accNo') {
      this.setState({
        accNo: e.target.value,
      })
    } else {
      this.setState({
        routingNumber: e.target.value,
      })
    }

  };


  /******************************Final Payment to cybersource *************************************/
  payNow = async () => {
    try {
      if(this.state.isChecked){
      let paymentRequest;
      var paymentData = new FormData();
      paymentData.append("bankAccountNumber", this.state.accNo);
      paymentData.append("bankAccountType", this.state.accType);
      paymentData.append("bankRoutingNumber", this.state.routingNumber);
      paymentRequest = await paymentClient.doAchPayment(paymentData);

      //***********************************if payment is sucess*******************************/
      if (paymentRequest.status == 200) {
        this.setState({
          showJsonData: JSON.stringify(paymentRequest.data, null, 2),
          isError:false
        })
      }
    }
    else{
      this.setState({
        isError: true,
      })
    }
    }
    catch (e) {
      console.log(e)
      //throw this.paymentError();
    }
  }


  isChecked=()=>{
    this.setState({
     isChecked: !this.state.isChecked
    })
  }

  render() {
    return (
      <>
        <Header />
        {/* Page content */}
        <Row className="mt-5">
         
            <Col xl="12">
              <Card className="shadow">
                <Row className="align-items-center">
                  <div className="col-6 mx-auto">
                    <h3 className="ml-4 text-center">ACH Payment</h3>
                    <Form role="form">
                      <FormGroup> <Label for="expMonth">
                        Bank Account number
                      </Label>
                        <InputGroup className="mb-3 w-100">

                          <Input placeholder="Account Number" id="accNo" type="text" name="accNo" onChange={(e) => this.update(e)} />
                        </InputGroup>
                      </FormGroup>
                      <FormGroup>
                        <Label for="routingNumber">
                          Bank Routing number
                        </Label>
                        <InputGroup className="mb-3 w-100">

                          <Input placeholder="Routing Number" name="routingNumber" id="routingNumber" type="text" onChange={(e) => this.update(e)} />
                        </InputGroup>
                      </FormGroup>
                      <FormGroup>
                        <InputGroup className="mb-3 w-100">
                          <Label for="expMonth">
                            Account Type
                          </Label>
                          <UncontrolledDropdown id="accType" className="w-100">
                            <DropdownToggle
                              caret
                              color="secondary"
                              id="dropdownMenuButton"
                              type="button"
                              className="w-50"
                            >
                              {this.state.accTypeName}
                            </DropdownToggle>
                            <DropdownMenu aria-labelledby="dropdownMenuButton">
                              <DropdownItem href="#pablo" value="C" name="accType" onClick={(e) => this.toggleOption(e)} >
                                Checking
                              </DropdownItem>
                              <DropdownItem href="#pablo" value="S" name="accType" onClick={(e) => this.toggleOption(e)} >
                                Saving
                              </DropdownItem>

                            </DropdownMenu>
                          </UncontrolledDropdown>
                        </InputGroup>
                      </FormGroup>
                      <Row><Col xl="6">
                        <FormGroup>
                          <input type="checkbox" className="checkbox" onChange={this.isChecked}></input>
                          <Label for="achpayment" className="ml-3">
                            ACH Payment
                          </Label>

                        </FormGroup>
                      </Col>
                      </Row>

                      <div id="errors-output" role="alert"></div>
                      <div className="text-center">
                        <Button id="pay-button" onClick={() => this.payNow()} className="my-4 btn-x-large" color="primary" type="button">
                          Make a Payment
                        </Button>
                      </div>
                      {this.state.isError 
                      &&
                      <h1> Please select the ach payment checkbox </h1>
                      }
                    </Form>
                  </div>
                </Row>
              </Card>
            </Col>
          

          <pre>{this.state.showJsonData}</pre>
        </Row>
      </>
    )
  }
}
AchPaymentWizard.layout = Admin;

export default AchPaymentWizard;
