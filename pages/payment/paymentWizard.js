import React from "react";
import paymentClient from "src/api/paymentClient";
import {
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown
} from "reactstrap";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  NavItem,
  NavLink,
  Nav,
  Progress,
  Table,
  Container,
  Row,
  Col,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup, Label
} from "reactstrap";
// layout for this page
import Header from "components/Headers/Header.js";
import Admin from "layouts/Admin.js";
import "assets/plugins/nucleo/css/nucleo.css";
class PaymentProcessor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      paymentStep: "card",
      FlexToken: '',
      mounted: false,
      capture: '',
      transactionID: "",
      orderId: "",
      responseMessage: "",
      success: 0,
      save: false,
      savedCard: null,
      cvv: null,
      error: null,
      expMonth: "Select Month",
      expYear: "Select Year"
    }

  };
  toggleOption = (e) => {
    if (e.target.name == "expMonth") {
      this.setState({
        expMonth: e.target.outerText,
      })
    }
    if (e.target.name == "expYear") {
      this.setState({
        expYear: e.target.outerText,
      })
    }
  };
  componentDidMount() {

  }
  componentWillMount() {
    this.generateKey();

  }
  generateKey = async () => {
    try {
      const pkeyReq = await paymentClient.generatePublicKey();

      // if (pkeyReq.data.status) {
        this.setState({
          capture: pkeyReq.data.keyId,
        })
        this.setState({
          paymentStep: "card",
        })
        this.flexTokenGenrate(pkeyReq.data.keyId);


      // }
    }
    catch (e) {
      //throw this.paymentError();
    }
  }
  paymentError = () => {
    this.setState({
      paymentStep: "error",
    })
  }
  flexTokenGenrate = (captureContext) => {
    let callback;
    var transientToken;
    var flex = new Flex(captureContext);
    var custom = {
      'input': {
        'font-size': '14px',
        'font-family': 'helvetica, tahoma, calibri, sans-serif',
        'color': '#555',
        'background': 'red'
      },
      ':focus': { 'color': 'blue' },
      ':disabled': { 'cursor': 'not-allowed' },
      'valid': { 'color': '#3c763d' },
      'invalid': { 'color': '#a94442' }
    };
    var microform = flex.microform({ styles: custom });
    var number = microform.createField('number', { placeholder: 'Enter card number',maxLength:'16' , styles: custom });
    var securityCode = microform.createField('securityCode', { placeholder: '•••' });
    number.load('#cardNumber');
    securityCode.load('#securityCode-container');


    //*************Click on Pay Button******************/


    var payButton = document.querySelector('#pay-button');
    payButton.addEventListener('click', async () => {
      var errorsOutput = document.querySelector('#errors-output');
      var options = {
        expirationMonth: this.state.expMonth,
        expirationYear: this.state.expYear
      };
      //**********************start promise**************/
      callback = new Promise((resolve, reject) => {
        microform.createToken(options, function (err, token) {

          //************************* handle error******************/
          if (err) {
            console.error(err);
            errorsOutput.textContent = err.message;
            reject(err.message)
            //throw this.paymentError();
          } else {
            //******/ At this point you may pass the token back to your server as you wish.
            // In this example we append a hidden input to the form and submit it. ********//


            var flexResponse = token;



            if (flexResponse) {

              resolve(flexResponse);


            }
            else {
              reject("No or Invalid Flex response")
            }
          }
        });
      });//********************************Promose end***************************/

      transientToken = await callback.then(result => result, error => {
        console.log(error)
        //throw this.paymentError();
      });

      this.confirmPay(transientToken)
    });

  }
  //*****************************************Payment confirmation*************************/
  confirmPay = (TransientToken) => {


    this.setState({
      FlexToken: TransientToken,
      paymentStep: "confirm",
    })

  }
  /******************************Final Payment to cybersource *************************************/
  payNow = async () => {
    try {
      let paymentRequest;
      // var paymentData = new FormData();
      // paymentData.append("flexresponse", this.state.FlexToken);
      paymentRequest = await paymentClient.payNow(this.state.FlexToken);
      console.log(paymentRequest)
      //***********************************if payment is sucess*******************************/
      if (paymentRequest.status == 200) {
        if (paymentRequest.data.status != "DECLINED") {
         
          if (paymentRequest.data.processorInformation.responseCode == "100") {
            this.setState({
              paymentStep: "success",
              transactionID: JSON.stringify(paymentRequest.data,null,2),
              success: 1,
              responseMessage: "Payment Successfull!"
            })
          }
          //********************************if payment failed case 3********************************************/
          else {
            this.setState({
              paymentStep: "success",
              success: 3,
              responseMessage: paymentRequest.data.data.errorInformation.message
            })

          }
        }
        else {
          this.setState({
            paymentStep: "success",
            success: 3,
            responseMessage: paymentRequest.data.data.errorInformation.message
          })
        }
      }
      else {
        this.setState({
          paymentStep: "success",
          success: 3,
          responseMessage: "Payment Failed. Please try again after sometime."
        })
      }
    }
    catch (e) {
      console.log(e)
      //throw this.paymentError();
    }
  }

  render() {
    return (
      <>
        <Header />
        {/* Page content */}
        <Row className="mt-5">
          {this.state.paymentStep == "card" && (
            <Col xl="12">
              <Card className="shadow">
                <Row className="align-items-center">
                  <div className="col-6 mx-auto">
                    <h3 className="ml-4 text-center">Quick Payment</h3>
                    <Form role="form">
                      <FormGroup> <Label for="expMonth">
                        Name
                      </Label>
                        <InputGroup className="mb-3 w-100">

                          <Input placeholder="Name" id="Name" type="text" />
                        </InputGroup>
                      </FormGroup>
                      <FormGroup>
                        <Label for="cardNumber">
                          Card Number
                        </Label>
                        <div type="text" className="form-control" id="cardNumber" placeholder="XXXX-XXXX-XXXX-XXXX"></div>
                      </FormGroup>
                      <Row><Col xl="6">
                        <FormGroup>
                          <InputGroup className="mb-3 w-100">
                            <Label for="expMonth">
                              Exp. Month
                            </Label>
                            <UncontrolledDropdown id="expMonth" className="w-100">
                              <DropdownToggle
                                caret
                                color="secondary"
                                id="dropdownMenuButton"
                                type="button"
                                className="w-100"
                              >
                                {this.state.expMonth}
                              </DropdownToggle>
                              <DropdownMenu aria-labelledby="dropdownMenuButton">
                                <DropdownItem href="#pablo" name="expMonth" onClick={(e) => this.toggleOption(e)} >
                                  01
                                </DropdownItem>
                                <DropdownItem href="#pablo" name="expMonth" onClick={(e) => this.toggleOption(e)} >
                                  02
                                </DropdownItem>
                                <DropdownItem href="#pablo" name="expMonth" onClick={(e) => this.toggleOption(e)} >
                                  03
                                </DropdownItem>
                                <DropdownItem href="#pablo" name="expMonth" onClick={(e) => this.toggleOption(e)} >
                                  04
                                </DropdownItem>
                                <DropdownItem href="#pablo" name="expMonth" onClick={(e) => this.toggleOption(e)} >
                                  05
                                </DropdownItem>
                                <DropdownItem href="#pablo" name="expMonth" onClick={(e) => this.toggleOption(e)} >
                                  06
                                </DropdownItem>
                                <DropdownItem href="#pablo" name="expMonth" onClick={(e) => this.toggleOption(e)} >
                                  07
                                </DropdownItem>
                                <DropdownItem href="#pablo" name="expMonth" onClick={(e) => this.toggleOption(e)} >
                                  08
                                </DropdownItem>
                                <DropdownItem href="#pablo" name="expMonth" onClick={(e) => this.toggleOption(e)} >
                                  09
                                </DropdownItem>
                                <DropdownItem href="#pablo" name="expMonth" onClick={(e) => this.toggleOption(e)} >
                                  10
                                </DropdownItem>
                                <DropdownItem href="#pablo" name="expMonth" onClick={(e) => this.toggleOption(e)} >
                                  11
                                </DropdownItem>
                                <DropdownItem href="#pablo" name="expMonth" onClick={(e) => this.toggleOption(e)} >
                                  12
                                </DropdownItem>
                              </DropdownMenu>
                            </UncontrolledDropdown>
                          </InputGroup>
                        </FormGroup>
                      </Col>
                        <Col xl="6">
                          <FormGroup>
                            <InputGroup className="mb-3 w-100">
                              <Label for="expYear">
                                Exp. Year
                              </Label>
                              {/* <Input value="GOODYEAR TIRE CENTER" type="text" /> */}
                              <UncontrolledDropdown id="expYear" className="w-100">
                                <DropdownToggle
                                  caret
                                  color="secondary"
                                  id="dropdownMenuButton"
                                  type="button"
                                  className="w-100"
                                >
                                  {this.state.expYear}
                                </DropdownToggle>
                                <DropdownMenu aria-labelledby="dropdownMenuButton">
                                  <DropdownItem href="#pablo" name="expYear" onClick={(e) => this.toggleOption(e)} >
                                    2022
                                  </DropdownItem>
                                  <DropdownItem href="#pablo" name="expYear" onClick={(e) => this.toggleOption(e)} >
                                    2023
                                  </DropdownItem>
                                  <DropdownItem href="#pablo" name="expYear" onClick={(e) => this.toggleOption(e)} >
                                    2024
                                  </DropdownItem>
                                  <DropdownItem href="#pablo" name="expYear" onClick={(e) => this.toggleOption(e)} >
                                    2025
                                  </DropdownItem>
                                  <DropdownItem href="#pablo" name="expYear" onClick={(e) => this.toggleOption(e)} >
                                    2026
                                  </DropdownItem>
                                  <DropdownItem href="#pablo" name="expYear" onClick={(e) => this.toggleOption(e)} >
                                    2027
                                  </DropdownItem>
                                  <DropdownItem href="#pablo" name="expYear" onClick={(e) => this.toggleOption(e)} >
                                    2028
                                  </DropdownItem>
                                </DropdownMenu>
                              </UncontrolledDropdown>
                            </InputGroup>
                          </FormGroup></Col></Row>
                      <FormGroup>
                        <Label for="securityCode-container">
                          CVV
                        </Label>
                        <div id="securityCode-container" className="form-control" placeholder="xxx" ></div>
                        <input type="hidden" id="flexresponse" value={this.state.capture} name="flexresponse" />
                      </FormGroup>
                      <div id="errors-output" role="alert"></div>
                      <div className="text-center">
                        <Button id="pay-button" className="my-4 btn-x-large" color="primary" type="button">
                          Make a Payment
                        </Button>
                      </div>
                    </Form>
                  </div>
                </Row>
              </Card>
            </Col>
          )}
          {this.state.paymentStep == "confirm" && (
            <Col xl="12">
              <Card className="shadow">
                <Row className="align-items-center">
                  <div className="col-6 mx-auto">
                    <h3 className="ml-4 text-center">Confirm Payment</h3>
                    <h6 className="text-center">Do you want to proceed to payment</h6>
                    <div className="text-center">
                      <Button onClick={() => this.payNow()} id="confirm-button" className="my-4 btn-x-large" color="primary" type="button">
                        Confirm
                      </Button>
                    </div>
                  </div>
                </Row>
              </Card>
            </Col>
          )}
          {this.state.paymentStep == "success" && (
            <Col xl="12">
              <Card className="shadow">
                <Row className="align-items-center">
                  <div className="col-6 mx-auto">
                    <h3 className="text-center">{this.state.responseMessage}</h3>
                    <center>
                      {this.state.success == 1 &&
                        <div className="mt-3 mb-3" style={{ boxShadow: '5px 5px 5px 0 rgb(0 0 0 / 13%), -5px -5px 5px 0 rgb(255 255 255 / 29%)', borderRadius: '50%', height: '50px', width: '50px', lineHeight: '50px', fontSize: '30px' }}>
                          <i className="ni ni-check-bold"></i>
                          
                        </div>
                      }
                      {this.state.success == 2 || this.state.success == 3 &&
                        <div className="mt-3 mb-3" style={{ boxShadow: '5px 5px 5px 0 rgb(0 0 0 / 13%), -5px -5px 5px 0 rgb(255 255 255 / 29%)', borderRadius: '50%', height: '50px', width: '50px', lineHeight: '50px', fontSize: '30px' }}>
                          <i className="ni ni-fat-remove" style={{ color: '#ea5353' }}></i>
                        </div>
                      }
                    </center>
                    {this.state.success != 3 ? (
                      <pre>{this.state.transactionID}</pre>)
                      : (<></>)}
                  </div>
                </Row>
              </Card>
            </Col>
          )}
        </Row>
      </>
    )
  }
}
PaymentProcessor.layout = Admin;

export default PaymentProcessor;
