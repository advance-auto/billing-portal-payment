import React from "react";
import paymentClient from "src/api/paymentClient";
import {
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown
} from "reactstrap";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  NavItem,
  NavLink,
  Nav,
  Progress,
  Table,
  Container,
  Row,
  Col,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup, Label
} from "reactstrap";
// layout for this page
import Header from "components/Headers/Header.js";
import Admin from "layouts/Admin.js";
import "assets/plugins/nucleo/css/nucleo.css";
class SavedCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      paymentStep: "card",
      FlexToken: '',
      mounted: false,
      capture: '',
      transactionID: "",
      orderId: "",
      responseMessage: "",
      success: 0,
      save: false,
      savedCard: null,
      cvv: null,
      error: null,
      expMonth: "Select Month",
      expYear: "Select Year",
      custToken:"D54F4E15EE0607BEE053AF598E0A2546",
      showJsonData:""
    }

  };
  toggleOption = (e) => {
    
  };
  
 

  //*****************************************Payment confirmation*************************/
  
  /******************************Final Payment to cybersource *************************************/
  payNow = async () => {
    try {
      let paymentRequest;
       var paymentData = new FormData();
       paymentData.append("customerToken", this.state.custToken);
      paymentRequest = await paymentClient.savedCardPayment(paymentData);

      //***********************************if payment is sucess*******************************/
      if (paymentRequest.status == 200) {
        console.log(paymentRequest)
        this.setState({
          showJsonData:JSON.stringify(paymentRequest.data,null,2),
        })
      }
    }
    catch (e) {
      console.log(e)
      //throw this.paymentError();
    }
  }

  render() {
    return (
      <>
        <Header />
        {/* Page content */}
        <Row className="mt-5">
          {this.state.paymentStep == "card" && (
            <Col xl="12">
              <Card className="shadow">
                <Row className="align-items-center">
                  <div className="col-6 mx-auto">
                    <h3 className="ml-4 text-center">Quick Payment</h3>
                    <Form role="form">
                     
                      <Row><Col xl="7">
                        <FormGroup>
                          <InputGroup className="mb-3 w-100">
                            <Label for="expMonth">
                             
                            </Label>
                            <UncontrolledDropdown id="expMonth" className="w-100">
                              <DropdownToggle
                                caret
                                color="secondary"
                                id="dropdownMenuButton"
                                type="button"
                                className="w-100"
                              >
                                {this.state.custToken}
                              </DropdownToggle>
                              <DropdownMenu aria-labelledby="dropdownMenuButton">
                                <DropdownItem href="#pablo" name="expMonth" onClick={(e) => this.toggleOption(e)} >
                                D54F4E15EE0607BEE053AF598E0A2546
                                </DropdownItem>
                                
                              </DropdownMenu>
                            </UncontrolledDropdown>
                          </InputGroup>
                        </FormGroup>
                      </Col>
                      </Row>
                     
                      <div id="errors-output" role="alert"></div>
                      <div className="text-center">
                        <Button onClick={() => this.payNow()} id="pay-button" className="my-4 btn-x-large" color="primary" type="button">
                          Pay Now
                        </Button>
                      </div>
                      
                    </Form>
                  </div>
                </Row>
                <pre>{this.state.showJsonData}</pre>
              </Card>
            </Col>
          )}
         
         
        </Row>
      </>
    )
  }
}
SavedCard.layout = Admin;

export default SavedCard;
