import React from "react";
import Link from "next/link";
// reactstrap components
import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarBrand,
  Navbar,
  NavItem,
  NavLink,
  Nav,
  Container,
  Row,
  Col,
} from "reactstrap";

function AdminNavbar() {
  return (
    <>
      <Navbar className=" navbar-horizontal navbar-dark p-0" style={{position: "sticky", top:" 0px",zIndex:"5555",background: "#cc0033" }} expand="md">
        <Container className="px-0">
          <Nav className="mr-auto" navbar>
            <NavItem>
              <Link href="/payment/paymentWizard">
                <NavLink href="#pablo" className="nav-link-icon">
                  <span className="nav-link-inner--text">HOME</span>
                </NavLink>
              </Link>
            </NavItem>
            <UncontrolledDropdown
              inNavbar
              nav
            >
              <DropdownToggle
                caret
                nav
              >
                PAYMENTS
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem href="/payments/invoices">
                  Make a Payment
                </DropdownItem>
                <DropdownItem href="/payments/payments">
                 Pending Payments
                </DropdownItem>
                <DropdownItem href="/payments/invoices">
                 Payment History
                </DropdownItem>
                <DropdownItem href="/payments/invoices">
                  Automatic Payments
                </DropdownItem>
                <DropdownItem href="/payments/invoices">
                  Saved Payment Cart
                </DropdownItem>
                {/* <DropdownItem divider /> */}
                <DropdownItem href="/payments/invoices">
                  Payment Sources
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
            <NavItem> 
              {/* <Link href="/auth/login"> */}
              <Link href="#">
                <NavLink href="#pablo" className="nav-link-icon">
                  <span className="nav-link-inner--text">STATEMENTS</span>
                </NavLink>
              </Link>
            </NavItem>
            <NavItem>
              {/* <Link href="/admin/profile"> */}
              <Link href="#">
                <NavLink href="#pablo" className="nav-link-icon">
                  <span className="nav-link-inner--text">MANAGE ACCOUNT</span>
                </NavLink>
              </Link>
            </NavItem>
          </Nav>
          
        </Container>
      </Navbar>
    </>
  );
}

export default AdminNavbar;
