import {
    Button,
    Card,
    CardHeader,
    CardBody,
    NavItem,
    NavLink,
    Nav,
    Progress,
    Table,
    Container,
    Row,
    Col,
    FormGroup,
    Form,
    Input,
    InputGroupAddon,
    InputGroupText,
    InputGroup,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown
} from "reactstrap"; 
function PaymentNavbar(props) {
    return (
        <>
            <Container fluid className={props.className}>
                <Row className="mb-2">
                    {props.tabs && props.tabs.map((tab,i)=>(
                    <Col xl="2" className="px-0" key={i}>
                        <div className="w-100 dropdown">
                            <button type="button" id="dropdownMenuButton" aria-haspopup="true" aria-expanded="false" className="w-100  btn btn-secondary">
                                {tab.name}
                            </button>
                        </div>
                    </Col>
                    ))}

                </Row>
            </Container>
        </>
    )
}
export default PaymentNavbar;
