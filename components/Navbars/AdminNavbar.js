import React from "react";
import Link from "next/link";
// reactstrap components
import {
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Form,
  FormGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  InputGroup,
  Navbar,
  Nav,
  Container,
  Media,
} from "reactstrap";

function AdminNavbar(props) {

  return (
    <>
      <Navbar style={{background:"#cc0033"}} className="navbar-top navbar-dark" expand="md" id="navbar-main">
        <Container fluid>
          
        <div className={'d-flex align-items-center'} style={{width:"500px"}}>   
      <img alt={props.logo.imgAlt} style={{width: "150px"}} className="navbar-brand-img" src={props.logo.imgSrc} />

      <Link href="/payment/paymentWizard">
            <a className="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block px-2">
              {props.brandText}
            </a>
          </Link>
          </div>
  
          {/* <Form className="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
            <FormGroup className="mb-0">
              <InputGroup className="input-group-alternative">
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>
                    <i className="fas fa-search" />
                  </InputGroupText>
                </InputGroupAddon>
                <Input placeholder="Search" type="text" />
              </InputGroup>
            </FormGroup>
          </Form> */}
          <Nav className="align-items-center d-none d-md-flex" navbar>
            {/* <UncontrolledDropdown nav> */}
              {/* <DropdownToggle className="pr-0" nav>
                <Media className="align-items-center">
                  <span className="avatar avatar-sm rounded-circle">
                    <img
                      alt="..."
                      src={require("assets/img/theme/team-4-800x800.jpg")}
                    />
                  </span>
                  <Media className="ml-2 d-none d-lg-block">
                    <span className="mb-0 text-sm font-weight-bold">
                      Admin
                    </span>
                  </Media>
                </Media>
              </DropdownToggle> */}
               {/* <Media className="align-items-center">
                  
                  <Media className="mr-2 ml-2 d-none d-lg-block">
                    <span className="mb-0 text-sm text-white font-weight-bold">
                    <i className="ni ni-support-16 text-white px-2"></i>
                   
                      Help
                    </span>
                  </Media>

                  <Media className="mr-2 ml-2 d-none d-lg-block align-items-center">
                 
                    <span className="mb-0 text-sm text-white font-weight-bold">
                    <i className="ni ni-settings-gear-65 text-white px-2"></i>
                      Preferences
                    </span>
                  </Media>

                  <Media className="mr-2 ml-2 d-none d-lg-block">
                 
                
                    <span className="mb-0 text-sm text-white font-weight-bold">
                    {/* <i className=" ni ni-bold-right text-white px-2"></i> */}
                      {/* Signout
                    </span>
                  </Media> 
                </Media> */}
              {/* <DropdownMenu className="dropdown-menu-arrow" right>
                <DropdownItem className="noti-title" header tag="div">
                  <h6 className="text-overflow m-0">Welcome!</h6>
                </DropdownItem>
                <Link href="/admin/profile">
                  <DropdownItem>
                    <i className="ni ni-single-02" />
                    <span>My profile</span>
                  </DropdownItem>
                </Link>
                <Link href="/admin/profile">
                  <DropdownItem>
                    <i className="ni ni-settings-gear-65" />
                    <span>Settings</span>
                  </DropdownItem>
                </Link>
                <Link href="/admin/profile">
                  <DropdownItem>
                    <i className="ni ni-calendar-grid-58" />
                    <span>Activity</span>
                  </DropdownItem>
                </Link>
                <Link href="/admin/profile">
                  <DropdownItem>
                    <i className="ni ni-support-16" />
                    <span>Support</span>
                  </DropdownItem>
                </Link>
                <DropdownItem divider />
                <DropdownItem href="#pablo" onClick={(e) => e.preventDefault()}>
                  <i className="ni ni-user-run" />
                  <span>Logout</span>
                </DropdownItem>
              </DropdownMenu> */}
            
          </Nav>
          <UncontrolledDropdown 
            >
              <DropdownToggle className="nav-drop-down"
              >
               Go To
              </DropdownToggle>
              <DropdownMenu right>
              <DropdownItem href="/payment/paymentWizard">
                CARD Payment
                </DropdownItem>
                <DropdownItem href="/payment/achPaymentWizard">
                  ACH Payment
                </DropdownItem> 
               
                <DropdownItem href="/payment/savedCardWizard">
                 Saved Card Payment
                </DropdownItem>
              
             
              </DropdownMenu>
            </UncontrolledDropdown>
        </Container>
      </Navbar>
    </>
  );
}

export default AdminNavbar;
